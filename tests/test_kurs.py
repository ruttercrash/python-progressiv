import test
from unittest.mock import patch


def test_hallo_welt(capsys):
    from aufgaben import a_hallo_welt
    std = capsys.readouterr()
    assert std.out == 'Hallo Welt\n'


# b_eingaben nicht testbar


def test_funktionen(capsys):
    from aufgaben import c_funktionen
    c_funktionen.input = lambda name: 'Marco'
    c_funktionen.hallo()
    std = capsys.readouterr()
    assert std.out == 'Hallo Marco\n'


def test_parameter(capsys):
    from aufgaben.d_parameter import hallo
    hallo('Manfred')
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred\n'


def test_hallo_terminal(capsys):
    from aufgaben.e_hallo_terminal import hallo
    hallo('Marco')
    std = capsys.readouterr()
    assert std.out == 'Hallo Marco\n'


def test_listen(capsys):
    from aufgaben.f_listen import hallo
    hallo(['Manfred', 'Marco'])
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred und Marco\n'


def test_string_methoden(capsys):
    from aufgaben.g_string_methoden import hallo
    hallo(['Manfred', 'Marco', 'Peter'])
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred und Marco und Peter\n'


def test_schleifen(capsys):
    from aufgaben.h_schleifen import hallo
    hallo(['Manfred', 'Marco', 'Peter'])
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred\nHallo Marco\nHallo Peter\n'


def test_beliebige_argumente(capsys):
    from aufgaben.i_beliebige_argumente import hallo
    hallo('Manfred', 'Marco', 'Peter', '...')
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred\nHallo Marco\nHallo Peter\nHallo ...\n'


def test_rueckgabewerte(capsys):
    from aufgaben.j_rückgabewerte import hallo
    anzahl = hallo('Manfred', 'Marco', 'Peter', '...')
    assert anzahl == 4


def test_default_arguments(capsys):
    from aufgaben.k_default_arguments import hallo
    hallo('Manfred', smalltalk=True)
    std = capsys.readouterr()
    assert std.out == 'Hallo Manfred. Na, wie läufts?\n'


