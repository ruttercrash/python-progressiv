"""
1. Bildschirmausgaben: print()-Funktion & Strings

Mit print() lassen sich Inhalte auf den Bildschrim ausgeben, z. B. Zahlen:
    >>> print(1)

Die drei >>> sind die Eingabeaufforderung der Python-Kosole. Die 1 ist die
Ausgabe der print()-Funktion auf den Bildschirm. Du kannst alle Beispiele
selbst in der Python-Konsole ausprobieren und damit experimentieren!

Wörter oder Sätze in '- oder "-Anführungszeichen nennt man Strings:
    >>> "String"
    'String'
    >>> 'Und ich bin auch 1 String :)'
    'Und ich bin auch 1 String :)'

Aufgabe:
    Gib den String 'Hallo Welt' auf den Bildschirm aus!

Teste ob Dein Ergebnis richtig ist, indem Du im Verzeichnis python-progressiv
den Befehl pytest eingibst (siehe Anleitung):
    $ ~/python-progressiv > pytest

Wenn es richtig war, sollte am Ende eine 1 in grüner Farbe stehen.
"""

# Dein Code hier!