"""
2. Variablen, Benutzereingaben & formatierte Strings

Mit dem Gleichheitszeichen kannst Du Bezeichnern (Variablen) einen Wert geben:
    >>> zahl = 42
    42

Mit der input()-Funktion kannst Du den Benutzer Deines Programms etwas
eingeben lassen. Die Eingabe wird dann in der Variablen zahl gespeichert:
    >>> zahl = input('Bitte eine Zahl eingeben: ')

Den Inhalt der Variablen kannst Du wieder mit der print()-Funktion ausgeben:
    >>> print(zahl)

Wenn Du ein kleines f vor einen String schreibst, kannst Du den Inhalt von
Variablen mit geschweiften Klammern {} in den String einfügen:
    >>> print(f'Zahl {zahl}')
    'Zahl 42'

Aufgabe:
    Frage den Benutzer nach seinem Namen und gib diesen auf den Bildschirm aus!

ACHTUNG: Für diese Aufgabe gibt es keinen Test zu Überprüfung!
         Den Grund dafür erfährst Du bei der nächsten Aufgabe...
"""