# Python Progressiv

## Es gibt unzählige Python Tutorials, warum noch eins? 

Die Idee stammt aus der Musik. Wenn Du ein Instrument nach Noten spielen lernst, übst Du am Anfang oft "progressive Etüden". Das sind kleine Übungsstücke, von denen jedes eine bestimmte Spieltechnik vermittelt. Es fängt ganz leicht an und wird mit jeder Etüde kaum merklich ein kleines bischen schwerer. Außerdem sind die Stücke so raffiniert komponiert, dass Du schon richtig schöne Musik damit machen kannst. Fazit: Du übst also nicht "stumpf" erst nur die Technik und machst später dann irgendwann mal Musik, sondern beides zugleich.

**Python Progressiv** möchte dieses sehr erfüllende System zu Lernen auf die Programmierung übertragen.


## Wie funktioniert das genau?

*"Progressiv"* bedeutet sowohl *"fortschreitend"* als auch *"fortschrittlich"*. 

**Fortschreitend:** Bei **Python Progressiv** kannst Du viele kleine Rätsel lösen. Du bekommst gerade so viele Hinweise, wie Du zur Lösung brauchst. Für jedes Rätsel gibt es einen automatischen Test. Am Anfang fallen alle Tests durch und zeigen ein rotes "F". Mit jedem Rätsel das Du löst, wird aus einem roten "F" ein kleiner grüner ".", wie wenn Du in einem Game ein Level geschafft hast. Die Rätsel zeigen Dir verschiedene Techniken der Programmierung und werden immer ein bischen schwieriger (fällt Dir was auf, genau wie bei den Etuden, oder? ;).

**Fortschrittlich:** In der modernen Programmierung ist neben der Beherrschung der Sprache auch die einiger Werkzeuge (tools) unerlässlich. Die beiden wichtigsten sind *"automatisches Testen"* (siehe oben) und *"verteilte Versionskontrolle"* (dazu später mehr). **Python Progressiv** macht Dich von Anfang an beim Erlernen der Sprache bereits mit beiden ein Stück vertraut. Damit soll Dir der Übergang vom Lernen hin zu "echten" Projekten, mit denen Du Deine eigenen Ideen verwirklichen kannst erleichtert werden.

Weiterhin bauen alle Rätsel aufeinander auf. Durch kleine Änderungen am Code Deines zuletzt gelösten Rätsels kannst Du das nächste Rätsel lösen. Dann kommt wieder der automatische Test zur Überprüfung. Dies ist genau die Methode, mit der die Profis riesige Programme mit Millionen Zeilen Code schreiben können, ohne dass ihnen das Ganze ständig um die Ohren fliegt. Und mit verteilter Versionskontrolle dann sogar noch gemeinsam mit hunderten oder gar tausenden Entwicklern, die rund um den Globus verstreut sind! Ziemlich cool, was? :)


## Benutzung

### Editor

Du kannst Pythonprogramme mit jedem "normalen" Texteditor schreiben. Mehr Spaß macht es aber mit einem speziell für Python entwickelten Editor. Und noch viel mehr Spaß für Anfänger macht es mit einem speziell für Pythonanfänger entwickelten Editor, dem Mu-Editor! 

Der Mu-Editor ist nicht nur *für* Python entwickelt, sondern ist auch selbst *in* Python geschrieben. Genauer gesagt in zur Zeit knapp 40.000 Zeilen Pythoncode von bislang 64 Programmieren. Die machen das aus Leidenschaft und haben wahrscheinlich kein Geld dafür bekommen. Und jetzt rate mal welche tools die Entwickler von Mu benutzen, um da den Überblick zu behalten :)

Das Beste ist Du kannst Mu kostenlos runterladen und benutzen (und sei ein bischen dankbar, okay)!

Geh auf diese Seite:	https://codewith.mu/en/download (Nur für Windows & Mac, in englisch)

Für Windows: Klicke auf "64-bit" neben dem Windowslogo

Für Mac: Der "Mac OSX Installer" neben dem Apfellogo funktioniert manchmal nicht, dann nimm PortaMu (darunter) und klicke auf "Mac OSX" neben dem USB-Stick

Für Linux: siehe unten


### Für debian-basiertes Linux wie Ubuntu oder Linux Mint:

Öffne ein Terminal (meist mit ```STRG-ALT-t```) und kopiere __jede Zeile einzeln__. Nach jeder Zeile ```ENTER``` drücken und abwarten. Die Zeilen die mit ```#``` anfangen sind Erklärungen für Dich, die brauchst Du nicht eingeben.

```bash
# Als erstes installieren wir einige Softwarepakete auf Deinem System
sudo apt update && apt upgrade -y
sudo apt install -y python3-pip build-essential libssl-dev libffi-dev python-dev python3-venv git

# Jetzt lädst Du Dir mit dem Versionskontrollprogramm 'git' das Projekt herunter
git clone https://gitlab.com/ruttercrash/python-progressiv.git

# Du gehst in das Verzeichnis, legst eine 'leere Entwicklungsumgebung (virtual env)' an und aktivierst diese
cd python-progressiv
python3 -m venv progressiv-env
. progressiv-env/bin/activate

# 'pip' ist der Installer für Pythonpakete, der die die notwendigen für dieses Projekt installiert
pip install --upgrade pip
pip install -r requirements-dev.txt
pip install mu-editor

# Mit pytest startest Du die automatischen Tests (s. o.), noch fallen alle durch :(
pytest
```
Wenn alles geklappt hat, solltest Du sowas ähnliches wie das hier auf Deinem Bildschirm sehen:

```
platform linux -- Python 3.8.2, pytest-5.4.1, py-1.8.1, pluggy-0.13.1
rootdir: /home/barrios/code/python-progressiv, inifile: pytest.ini
collected 10 items                                                                                                            

tests/test_kurs.py FFFFFFFFFF                                                                                           [100%]

=================================================== short test summary info ===================================================
FAILED tests/test_kurs.py::test_hallo_welt - AssertionError: assert '' == 'Hallo Welt\n'
FAILED tests/test_kurs.py::test_funktionen - AttributeError: module 'aufgaben.c_funktionen' has no attribute 'hallo'
FAILED tests/test_kurs.py::test_parameter - ImportError: cannot import name 'hallo' from 'aufgaben.d_parameter' (/home/barri...
FAILED tests/test_kurs.py::test_hallo_terminal - ImportError: cannot import name 'hallo' from 'aufgaben.e_hallo_terminal' (/...
FAILED tests/test_kurs.py::test_listen - ImportError: cannot import name 'hallo' from 'aufgaben.f_listen' (/home/barrios/cod...
FAILED tests/test_kurs.py::test_string_methoden - ImportError: cannot import name 'hallo' from 'aufgaben.g_string_methoden' ...
FAILED tests/test_kurs.py::test_schleifen - ImportError: cannot import name 'hallo' from 'aufgaben.h_schleifen' (/home/barri...
FAILED tests/test_kurs.py::test_beliebige_argumente - ImportError: cannot import name 'hallo' from 'aufgaben.i_beliebige_arg...
FAILED tests/test_kurs.py::test_rueckgabewerte - ImportError: cannot import name 'hallo' from 'aufgaben.j_rückgabewerte' (/h...
FAILED tests/test_kurs.py::test_default_arguments - ImportError: cannot import name 'hallo' from 'aufgaben.k_default_argumen...
===================================================== 10 failed in 0.23s ======================================================
```

```bash
# Starte ǹun Deinen Mu-Editor und gib den Cursor wieder frei danach (&-Zeichen am Ende)
mu-editor &
```